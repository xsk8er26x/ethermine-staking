let entrance = document.getElementById("balances__modal");
let entranceSecond = document.getElementById("balances__modal-system");

window.onclick = function (event) {
  if (event.target == entrance) {
    entrance.style.display = "none";
  } else if (event.target == entranceSecond) {
    entranceSecond.style.display = "none";
  }
};

window.addEventListener("keydown", (e) => {
  if (e.key === "Escape") {
    if (entranceSecond.style.display == "block") {
      entranceSecond.style.display = "none";
    } else {
      entrance.style.display = "none";
    }
  }
});

let input = document.getElementById("search-input");

input.addEventListener("focus", function () {
  input.style.width = "440px";
});

input.addEventListener("focusout", function () {
  if (input.value.trim() == "") {
    input.style.width = "180px";
  }
});
